class Slave{
	name;
	age;
	latitude;
	longitude;
	alienReview;
	ghostReview;
	bananaReview;
	carrotReview;
	catReview;
	strawberryReview;
	hoursWorked;
	kmsWalked;
	foodCost;
	restTime;
	efficiency;
	hp;
	weight;
	height;
	bornDate;
	bornPlace;

	constructor(name, age, latitude, longitude, hoursWorked, kmsWalked, foodCost, restTime, efficiency, hp, weight, height, bornDate){
		this.name = name;
		this.age = age;
		this.latitude = latitude;
		this.longitude = longitude;
		this.alienReview = 0;
		this.ghostReview = 0;
		this.bananaReview = 0;
		this.carrotReview = 0;
		this.catReview = 0;
		this.strawberryReview = 0;
		this.hoursWorked = hoursWorked;
		this.kmsWalked = kmsWalked;
		this.foodCost = foodCost;
		this.restTime = restTime;
		this.efficiency = efficiency;
		this.hp = hp;
		this.weight = weight;
		this.height = height;
		this.bornDate = bornDate;
		this.bornPlace = "unknown";
	}

	set latitude(lat){
		this.latitude = lat;
	}

	set longitude(lng){
		this.latitude = lng;
	}

	set alienReview(count){
		this.alienReview = count;
	}

	set ghostReview(count){
		this.ghostReview = count;
	}

	set bananaReview(count){
		this.bananaReview = count;
	}

	set carrotReview(count){
		this.carrotReview = count;
	}

	set catReview(count){
		this.catReview = count;
	}

	set strawberryReview(count){
		this.strawberryReview = count;
	}
}

// var slaves = [
// 	new Slave("vienas", 10, 51.508742, -0.120850),
// 	new Slave("du", 20, 51.608742, 0.120850),
// 	new Slave("trys", 20, 51.518742, 0.020850)
// 	];

var slaves = JSON.parse(localStorage.getItem('slaves'));

function addSlave(newSlave){
	slaves.push(newSlave);
}
	
function getSlaves(){
	return slaves;
}

function getSlave(index){
	return slaves[index];
}

function getByName(name){
	return slaves.find(x => x.name === name);
}

function replaceSlave(newSlave){
	var index = slaves.indexOf(newSlave);
	if (index !== -1) {
		slaves[index] = newSlave;
	}
	localStorage.setItem('slaves', JSON.stringify(slaves));
}

function changeSlaveLocation(slave){
	alert("Paspauskite ant norimos vietos žemėlapyje")
	removeMarkers();
	setBoolean(true);
	google.maps.event.addListener(map, 'click', function(event) {
		placeMarker(event.latLng, slave);		
	});	
}

function slaveLocation(slave, lat, lng){

	newSlave = getByName(slave.name);
	newSlave.latitude = lat;
	newSlave.longitude = lng;

	var index = slaves.indexOf(slave);
	if (index !== -1) {
		slaves[index] = newSlave;
	}

	localStorage.setItem('slaves', JSON.stringify(slaves));
}

// function setSessionItem(name, value) {
// 	var mySession;
// 	try {
// 		mySession = JSON.parse(localStorage.getItem('mySession'));
// 	} catch (e) {
// 		console.log(e);
// 		mySession = {};
// 	}

// 	mySession[name] = value;

// 	mySession = JSON.stringify(mySession);

// 	localStorage.setItem('mySession', mySession);
// }

// function getSessionItem(name) {
//     var mySession = localStroage.getItem('mySession');
//     if (mySession) {
//         try {
//             mySession = JSON.stringify(mySession);
//             return mySession[name];
//         } catch (e) {
//             console.log(e);
//         }
//     }
// }