var map;
var marker;
var waitingForMarker = false;

function initMap(center) {
	map = new google.maps.Map(
		document.getElementById('map'), {zoom: 10, center: center});	
}

function setBoolean(value){
	waitingForMarker = value;
}

function drawMarker(latitude, longitude){
	var position = {lat: latitude, lng: longitude};
	  
	marker = new google.maps.Marker({
	position: position,
	icon: {
		path: google.maps.SymbolPath.CIRCLE,
		scale: 10}});

	marker.setMap(map);

	setBoolean(false);
}

function removeMarkers(){
	marker.setMap(null);
}

function placeMarker(location, slave) {
	if(waitingForMarker){
		marker = new google.maps.Marker({
			position: location, 
			icon: {
				path: google.maps.SymbolPath.CIRCLE,
				scale: 10}
		});
		marker.setMap(map);
		setBoolean(false);

		//console.log(marker.getPosition().lat());
		slaveLocation(slave, marker.getPosition().lat(), marker.getPosition().lng())
	}	
}